module.exports = {
    siteMetadata: {
        title: 'Wordpress Gatsby',
        subtitle: `Gatsby wordpress starter`,
    },
    plugins: [
        {
            resolve: "gatsby-source-wordpress",
            options: {
                baseUrl: "strangest.nataliegustafsson.fi",
                protocol: "http",
                hostingWPCOM: false,
                useACF: true,
                verboseOutput: true
            },
        },
    ],
};
