import React from "react"
import Header from '../components/header.jsx'
import '../styles/style.css'
import { Link } from 'gatsby'
import PropTypes from 'prop-types'
import RecentPosts from '../components/recentPosts.jsx'
import CategoryList from '../components/categoryList.jsx'


class PostsTemplate extends React.Component{
  render(){
    const data = this.props.data
    console.log(data)
    return(
      <div>
        <Header />
        <div className="blog">
          <div className="posts">
            <h4 style={{paddingBottom: "35px"}}>POSTS</h4>
            {data.allWordpressPost.edges.map(({node}) => (
              <div key={node.slug} style={{paddingBottom: "50px"}}>

                  <p className="date">{node.date}</p>

                  <Link to={'/post/' + node.slug}>
                    <h4 className="postTitle">{node.title}</h4>
                  </Link>

                  {node.featured_media &&
                    <div>
                     <img style={{width: "100%"}} src={node.featured_media.source_url}/>
                     </div>
                   }

                <div style={{width: '100%'}} className="content" dangerouslySetInnerHTML={{__html: node.content}} />
              </div>
            ))}
          </div>
          <div className="sidebar">
            <RecentPosts />
            <CategoryList />
          </div>
        </div>
      </div>
    )
  }
}

export default PostsTemplate

export const pageQuery = graphql`
    query postsQuery{
        allWordpressPost {
            edges{
              node{
                id
                title
                excerpt
                content
                slug
                date(formatString: "MMMM DD, YYYY")
                featured_media{
                  source_url
                }
                categories{
                  name
                  slug
                }
                tags{
                  name
                  slug
                }
              }
            }
        }
    }
`
