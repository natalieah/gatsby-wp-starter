import React from "react"
import { StaticQuery, graphql } from "gatsby"
import Link from 'gatsby-link'

export default class recentPosts extends React.Component{
  render(){
    return(
      <StaticQuery
        query={graphql`
          query recentQuery {
            allWordpressPost {
              edges {
                node{
                  title
                  slug
                }
              }
            }
          }
        `}
        render={data => (
          <div>
            <h5>RECENT POSTS</h5>
            {data.allWordpressPost.edges.slice(0, 5).map(({node}) => (
              <div key={node.slug}>
                <ul className="sidebar-list">
                  <li>
                    <Link to={'/post/' + node.slug}>
                      {node.title}
                    </Link>
                  </li>
                </ul>
              </div>
            ))}
          </div>
        )}
      />
    )
  }
}
