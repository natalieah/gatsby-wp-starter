import React from "react"
import { StaticQuery, graphql } from "gatsby"
import Link from 'gatsby-link'

export default class categoryList extends React.Component{
  render(){
    return(
      <StaticQuery
        query={graphql`
          query categoryQuery {
            allWordpressCategory {
              edges {
                node {
                  id
              		name
                  slug
                }
              }
            }
          }
        `}
        render={data => (
          <div>
            <h5 style={{paddingTop: "25px"}}>CATEGORIES</h5>
            {data.allWordpressCategory.edges.map(({node}) => (
              <div key={`${node}cat`}>
                <ul className="sidebar-list">
                  <li>
                    <Link to={`/categories/${node.slug}/`}>
                        {node.name}
                    </Link>
                  </li>
                </ul>
              </div>
            ))}
          </div>
        )}
      />
    )
  }
}
