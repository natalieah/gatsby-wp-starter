import React from "react"
import { StaticQuery, graphql } from "gatsby"

const headerImage = 'https://images.unsplash.com/photo-1540677326198-bfecf46dd8a9?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=c67cc66eb82374e87987518d1a33909a&auto=format&fit=crop&w=1568&q=80'

export default () => (
  <StaticQuery
    query={graphql`
      query HeadingQuery {
        site {
          siteMetadata {
            title
            subtitle
          }
        }
      }
    `}
    render={data => (
      <div className="header">
        <div className="siteTitle">
          <a href='http://happy-cakes.surge.sh'>
            <h1 style={{color: '#fff'}}>{data.site.siteMetadata.title}</h1>
          </a>
          <p>{data.site.siteMetadata.subtitle}</p>
        </div>
        <img src={headerImage}></img>
      </div>
    )}
  />
)
