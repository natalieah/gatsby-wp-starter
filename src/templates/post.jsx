import React, { Component } from "react"
import { Link } from 'gatsby'
import Header from '../components/header.jsx'
import RecentPosts from '../components/recentPosts.jsx'
import CategoryList from '../components/categoryList.jsx'

class PostTemplate extends Component {
    render() {
        const post = this.props.data.wordpressPost
        return (
          <div>
            <Header />
            <div className="blog">
              {post.featured_media &&
                <div>
                 <img style={{width: '100%', paddingBottom: '50px'}} src={post.featured_media.source_url}/>
                 </div>
               }
              <div className="posts">
                  <p className="date" dangerouslySetInnerHTML={{ __html: post.date }} />
                  <h4 className="postTitle" dangerouslySetInnerHTML={{ __html: post.title }} />
                  <div dangerouslySetInnerHTML={{ __html: post.content }} />
              </div>
              <div className="sidebar">
                <RecentPosts />
                <CategoryList />
              </div>
            </div>
          </div>
        )
    }
}
export default PostTemplate

export const pageQuery = graphql`
    query currentPostQuery($id: String!) {
        wordpressPost(id: { eq: $id }) {
            title
            content
            slug
            date(formatString: "MMMM DD, YYYY")
            featured_media {
              source_url
            }
            categories{
              name
              slug
            }
        }
        site {
            siteMetadata {
                title
                subtitle
            }
        }
    }
`
