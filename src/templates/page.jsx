import React, {Component} from "react"
import Header from '../components/header.jsx'
import RecentPosts from '../components/recentPosts.jsx'
import CategoryList from '../components/categoryList.jsx'


class PageTemplate extends Component {
    render() {
        const siteMetadata = this.props.data.site.siteMetadata
        const currentPage = this.props.data.wordpressPage

        console.log(currentPage)

        return (
            <div>
              <Header />
              <div className='blog'>
                <div className='posts'>
                  <h2 dangerouslySetInnerHTML={{__html: currentPage.title}}/>
                  <div dangerouslySetInnerHTML={{__html: currentPage.content}}/>
                </div>
                <div className="sidebar">
                  <RecentPosts />
                  <CategoryList />
                </div>
              </div>
            </div>
        )
    }
}

export default PageTemplate

export const pageQuery = graphql`
    query currentPageQuery($id: String!) {
        wordpressPage(id: { eq: $id }) {
            title
            content
            slug
            id
            date(formatString: "MMMM DD, YYYY")
        }
        site {
            id
            siteMetadata {
                title
                subtitle
            }
        }
    }
`
