import React from 'react'
import { Link, graphql } from 'gatsby'
import Header from '../components/header.jsx'
import RecentPosts from '../components/recentPosts.jsx'
import CategoryList from '../components/categoryList.jsx'

class categoriesTemplate extends React.Component {
  render(){
    const posts = this.props.data.allWordpressPost.edges
    const postList = posts.map(post => (
      <div  style={{float: 'right'}} className="posts" key={post.node.slug}>
        <p className="date">{post.node.date}</p>
        <Link to={'/post/' + post.node.slug}>
          <h4 className="postTitle">{post.node.title}</h4>
        </Link>
        {post.node.featured_media &&
          <div>
           <img style={{width: "100%"}} src={post.node.featured_media.source_url}/>
           </div>
         }
         <div dangerouslySetInnerHTML={{ __html: post.node.content }} />
      </div>
    ))
    const category = this.props.pageContext.name

    return(
      <div>
        <Header />
        <div className="blog">
         <div className="sidebar" style={{float: 'left'}}>
           <p className="date" style={{color: '#000'}}>Category: {category}</p>
         </div>
         <div style={{float: 'right'}}>
           {postList}
         </div>
        </div>
      </div>
    )
  }
}

export default categoriesTemplate


export const pageQuery = graphql`
query CategoryPage($slug: String!) {
site {
  siteMetadata {
    title
  }
}
allWordpressPost(filter: { categories: { slug: { eq: $slug } } }) {
  edges {
    node {
      id
      title
      excerpt
      content
      slug
      date(formatString: "MMMM DD, YYYY")
      featured_media{
        source_url
      }
      categories{
        name
      }
    }
  }
}
}

`
