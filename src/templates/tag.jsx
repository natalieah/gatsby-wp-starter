import React from 'react'
import { Link, graphql } from 'gatsby'

class tagsTemplate extends React.Component {
  render(){
    const posts = this.props.data.allWordpressPost.edges
    const postList = posts.map(post => (
      <li key={post.node.slug}>
        <Link to={'/post/' + post.node.slug}>
          <h2>{post.node.title}</h2>
        </Link>
      </li>
    ))
    const tag = this.props.pageContext.name

    return(
      <div>
        <h1>Tag: {tag}</h1>
        <ul>{postList}</ul>
      </div>
    )
  }
}

export default tagsTemplate


export const pageQuery = graphql`
query TagPage($slug: String!) {
site {
  siteMetadata {
    title
  }
}
allWordpressPost(filter: { tags: { slug: { eq: $slug } } }) {
  edges {
    node {
      id
      title
      excerpt
      slug
      date(formatString: "MMMM DD, YYYY")
      featured_media{
        source_url
      }
      categories{
        name
      }
      tags{
        name
      }
    }
  }
}
}

`
